var browserify = require('browserify')
var browserSync = require('browser-sync'); 
var concat = require('gulp-concat');
var convertEncoding = require('gulp-convert-encoding');
var gulp = require('gulp')
var proxy = require('proxy-middleware');
var source = require('vinyl-source-stream')
var tsify = require('tsify')
var url = require('url');

const paths =  {
  dist: ['./dist/**/*.*', '!./node_modules/**/*'],
  html: ['src/**/*.html'],
  css: ['src/**/*.css']
};

const host = 'http://dsv.syonet.com:41221';

function copyHtml(encoding) {
  console.log( "Copiando arquivos HTML -> encoding: " + encoding );
  return gulp.src(paths.html)
    .pipe(gulp.dest('dist'))
}

function concatCss(encoding) {
  console.log( "Concatenando arquivos CSS -> encoding: " + encoding );

  return gulp.src(paths.css)
    .pipe(concat('main.css'))
    .pipe(convertEncoding({to: encoding}))
    .pipe(gulp.dest('dist'))
}

function convertTStoJS ( encoding ) {
  console.log( "Convertendo TS para JS -> encoding: " + encoding );
  return browserify({
    basedir: '.',
    debug: true,
    entries: ['src/main.ts'],
    cache: {},
    packageCache: {}
  })
  .plugin(tsify)
  .transform('babelify', {
    presets: ['es2015'],
    extensions: ['.ts']
  })
  .bundle()
  .on('error', function(err) {
    console.log(err.message);
    this.emit('end');
  })
  .pipe(source('bundle.js'))
  .pipe(convertEncoding({to: encoding}))
  .pipe(gulp.dest('dist'))
}

gulp.task('ts', () => {
  convertTStoJS( 'utf-8' );
});

gulp.task('css', () => {
  concatCss( 'utf-8' );
});

gulp.task('html', () => {
  copyHtml( 'utf-8' );
});

gulp.task('dev', () => {
  const iso = 'utf-8';
  copyHtml( iso );
  concatCss( iso );
  convertTStoJS( iso );
});

gulp.task('watch', () => {
  gulp.watch('./src/**/*.ts', ['ts']);
  gulp.watch('./src/**/*.css', ['css']);
  gulp.watch('./src/**/*.html', ['html']);
  gulp.watch(paths.dist, ['reload']);
});

gulp.task('prod', () => {
  const iso = 'iso-8859-1';
  copyHtml( iso );
  concatCss( iso );
  convertTStoJS( iso );
});

gulp.task('browser-sync', function() {
  const proxyOptionsAPI = url.parse( host + '/api');
  proxyOptionsAPI.route = '/api';

  const proxyOptionsPortal = url.parse(host + '/portal');
  proxyOptionsPortal.route = '/portal';
  browserSync({
      open: true,
      port: 3000,
      server: {
          baseDir: "./dist",
          middleware: [
            proxy(proxyOptionsAPI),
            proxy(proxyOptionsPortal)
          ]
      }
  });
});

gulp.task('reload', function() {
    gulp.src(paths.dist)
        .pipe(browserSync.reload({stream: true}));
});

gulp.task('default', ['browser-sync', 'watch']);