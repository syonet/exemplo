import { ParametroService } from './ParametroService';

export class ServiceInject {
  public static register(app: ng.IModule): ng.IModule {
    console.log(1)
    return app
      .service('parametroService', ParametroService)
  }
}
