import { IParametro } from "../value/IParametro";

export class ParametroService {
    static $inject: string[] = ['$http'];
    private url: string = '/api/parametro/';
    constructor(private $http: ng.IHttpService) { }
  
    list(): ng.IHttpPromise<Array<IParametro>> {
      return this.$http.get(this.url);
    }
}