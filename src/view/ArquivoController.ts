import { SyoView, State, Data } from '../syonet/core'
import { IStateService } from 'angular-ui-router'
import { ParametroService } from '../service/ParametroService'
import { IParametro } from '../value/IParametro'

@SyoView({ 
  templateUrl: 'view/ArquivoController.html', 
  state: 'historicoCliente',
  controllerAs: 'ArquivoCtrl',
  url: '/historico/cliente' 
}, {
  group: 'Guardian',
  title: 'TITULO',
  icon: 'user'
})

export class ArquivoController implements ng.IController {
  public static $inject: string[] = [
    '$state',
    'parametroService'
  ]
  private _title: string
  private _parametros : Array<IParametro>;
  
  public get parametros() : Array<IParametro> {
    return this._parametros;
  }
  public set parametros(v : Array<IParametro>) {
    this._parametros = v;
  }
  public get title(): string {
    return this._title
  }
  public set title(v: string) {
    this._title = v
  }
  constructor(
    private $state: IStateService & { current: { data: Data } },
    private parametroService: ParametroService
  ) {
    this.title = $state.current.data.title
    this.parametroService.list().then( response => this.parametros = response.data )
  }
}
