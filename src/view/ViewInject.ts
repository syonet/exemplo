import { ArquivoController } from './ArquivoController'
import { config, State } from './../syonet/core'
import { IUrlRouterProvider, IStateProvider } from 'angular-ui-router'
import { TraducaoFactory } from '../translate/TraducaoFactory'

export class ViewInject {
  public static register(app: ng.IModule): ng.IModule {
    return app
      .controller('ArquivoController', ArquivoController)
      .config(['$urlRouterProvider', '$stateProvider', '$translateProvider',(
        $urlRouterProvider: IUrlRouterProvider , 
        $stateProvider: IStateProvider,
        $translateProvider: angular.translate.ITranslateProvider ) => {
        $translateProvider.useSanitizeValueStrategy('escape')
        $translateProvider.preferredLanguage('PT-BR')
        $translateProvider.useLoader('customLoader')
        config.forEach((value: State, key: string) => {
          $stateProvider.state(key, value)
        })
        $urlRouterProvider.otherwise( '/historico/cliente' );
      }])
      .factory('customLoader', ($q: angular.IQService, $http: angular.IHttpService) => {
        return (options: any) => {
          const url: string = '/api/parametro/'
          const promise = $http.get(url + 'IDIOMA_SISTEMA')
          let dfd: angular.IDeferred<any> = $q.defer()
          promise.then((response: any) => {
            dfd.resolve(new TraducaoFactory().create(response.data.valor))
          })
          return dfd.promise
        }
      })
  }
}
