import { create } from 'domain'
import { ITraducao } from './ITraducao'
import { Portugues } from './Portugues'
import { Espanhol } from './Espanhol'

export class TraducaoFactory {
    create(idioma: string): ITraducao {
        switch (idioma) {
            case 'PT-BR':
                return new Portugues()
            case 'ES':
                return new Espanhol()
            default:
                return new Portugues()
        }
    }
}
