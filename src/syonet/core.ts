
/**
 * Decorator para montar a config do state
 * @param prop 
 */
export function SyoView(prop: State, data?: Data) {
  return function (target: ng.Injectable<ng.IControllerConstructor> & Function ) {
    if (!prop.controller) {
      prop.controller = target.name
    }
    prop.data = data
    config.set(prop.state, prop)
    config = new Map([...config.entries()].sort())
  }
}

export interface State {
  controller?: string,
  controllerAs?: string,
  templateUrl: string,
  state: string,
  name?: string,
  url: string,
  data?: Data
  initializing?: boolean
}

export interface Data {
  group: string,
  title: string,
  icon: string,
}

export let config: Map<string,State> = new Map()
