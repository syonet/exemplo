import { CamelCaseToStrFilter } from './CamelCaseToStrFilter'
import { FromMap } from './FromMap'

export class FilterInject {
  public static register(app: ng.IModule): ng.IModule {
    return app
      .filter('camelCase', new CamelCaseToStrFilter().filtro)
      .filter('fromMap', new FromMap().filtro)
  }
}
