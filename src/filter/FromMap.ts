export class FromMap {
  public filtro() {
    return (input: Map<any, any>): any => {
      let out: any = {}
      input.forEach((v, k) => out[k] = v)
      return out
    }
  }
}
