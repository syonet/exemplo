export class CamelCaseToStrFilter {
  public filtro() {
    return (text: any): any => {
      let result = text.replace(/([A-Z])/g, ' $1')
      return result.charAt(0).toUpperCase() + result.slice(1)
    }
  }
}
