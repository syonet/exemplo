
import { SidebarComponent } from './SidebarComponent'

export class ComponentInject {
  public static register(app: ng.IModule): ng.IModule {
    return app.directive( 'sidebar', SidebarComponent.factory )
  }
}
