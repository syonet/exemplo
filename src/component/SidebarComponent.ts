import { IStateService, IState } from 'angular-ui-router'
import { IScope } from 'angular'
import { State } from '../syonet/core'

export class SidebarComponent implements ng.IDirective {
  public restrict: string = 'E'
  public controllerAs: string = 'ModuleCtrl'
  public controller = class ModuleController implements ng.IController {
    public static $inject: string[] = ['$state', '$scope', '$dialog', '$window']

    private _secaoAtual: any = {}
    public get secao(): any {
      return this._secaoAtual
    }
    public set secao(v: any) {
      this._secaoAtual = v
    }

    private _sessaoGroups: Map<string, any[]>
    public get sessaoGroups(): Map<string, any[]> {
      return this._sessaoGroups
    }
    public set sessaoGroups(v: Map<string, any[]>) {
      this._sessaoGroups = v
    }
    constructor(
      private $state: IStateService,
      private $scope: IScope,
      private $dialog: any,
      private $window: Window) {
      this.buildSecoes()
      this.$scope.$on('$stateChangeSuccess', (evt: any, toState: any) => {
        this.sessaoGroups.forEach((group, key) => {
          const secao = group.find((el: any) => el.name === toState.name)
          if (secao) {
            secao.initializing = false
          }
        })
      })
    }
    backState() {
      this.$window.history.back()
    }
    checkSessao(secao: State): boolean {
      if (this.$state.current.data) {
        this._secaoAtual = this.$state.current
      }
      return secao.data === this._secaoAtual.data
    }

    buildSecoes() {
      this.sessaoGroups = new Map<string, any[]>()
      const secoes: IState[] = this.$state.get()
      secoes.forEach((secao: IState) => {
        let data = secao.data
        if (!data) {
          return
        }
        let isSection = secao.name && data && data.group
        if (!isSection) {
          return
        }
        this.sessaoGroups.set(data.group, this.sessaoGroups.get(data.group) || [])
        this.sessaoGroups.get(data.group).push({
          name: secao.name,
          state: secao.name,
          data: data,
          initializing: false
        })
      })
    }

    ajuda() {
      let ajudaDialog = this.$dialog.create({
        title: 'Ajuda',
        template: '<iframe class="googledocs-iframe" src="https://google.com"></iframe>',
        modal: true,
        width: 700,
        height: 340
      })
      ajudaDialog.open()
    }
  }

  public templateUrl: string = 'component/SidebarComponent.html'
  public static factory(): ng.IDirective {
    return new SidebarComponent()
  }
}
