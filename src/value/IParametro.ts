export interface IParametro {
    idParametro: string,
    agrupamento: string,
    valor: string,
    _url: string
}