import { ViewInject } from './view/ViewInject'
import { ComponentInject }  from './component/ComponentInject'
import { FilterInject } from './filter/FilterInject'
import { ServiceInject } from './service/ServiceInject';

function load ( window: any ) {
  let angular = window.angular 
  if ( !angular ) {
    return
  }
  let app: ng.IModule = angular.module('app', [
    'syonet',
    'syonetResource',
    'ui.router',
    '$moduleWorkspace',
    'syonet.namebeautifier',
    'ui.utils',
    'pascalprecht.translate'
  ])
  FilterInject.register( app )
  ComponentInject.register( app )
  ViewInject.register( app )
  ServiceInject.register( app )
}

load( window )
