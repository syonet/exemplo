## Auditoria

O modulo exibe detalhes sobre o processamento de arquivos na atualizacao.

Este modulo foi desenvolvido com AngularJS e TypeScript.
Este modulo depende do modulo angular-services

## Como instalar
npm install


## Requisitos
Servidor com o syonetCRM


## Desenvolvimento
O desenvolvimento pode ser feito separado do SYONET CRM, inclusive apontando para endereços de produção.

1.  Apontar uma URL valida no arquivo gulpfile.js, essa url será usada como proxy para redirecionar os requests.
1. `npm run dev` executa o build para executar separadamente
1. `npm start` inicia a aplicacao na porta 3000


## Publicacao
`npm run prod` executa o build de producao que será inserido no syonetCRM. CollaborativePORTAL/portal/auditoria


