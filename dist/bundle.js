(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", { value: true });
var SidebarComponent_1 = require("./SidebarComponent");

var ComponentInject = function () {
    function ComponentInject() {
        _classCallCheck(this, ComponentInject);
    }

    _createClass(ComponentInject, null, [{
        key: "register",
        value: function register(app) {
            return app.directive('sidebar', SidebarComponent_1.SidebarComponent.factory);
        }
    }]);

    return ComponentInject;
}();

exports.ComponentInject = ComponentInject;

},{"./SidebarComponent":2}],2:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", { value: true });

var SidebarComponent = function () {
    function SidebarComponent() {
        _classCallCheck(this, SidebarComponent);

        var _a;
        this.restrict = 'E';
        this.controllerAs = 'ModuleCtrl';
        this.controller = (_a = function () {
            function ModuleController($state, $scope, $dialog, $window) {
                var _this = this;

                _classCallCheck(this, ModuleController);

                this.$state = $state;
                this.$scope = $scope;
                this.$dialog = $dialog;
                this.$window = $window;
                this._secaoAtual = {};
                this.buildSecoes();
                this.$scope.$on('$stateChangeSuccess', function (evt, toState) {
                    _this.sessaoGroups.forEach(function (group, key) {
                        var secao = group.find(function (el) {
                            return el.name === toState.name;
                        });
                        if (secao) {
                            secao.initializing = false;
                        }
                    });
                });
            }

            _createClass(ModuleController, [{
                key: "backState",
                value: function backState() {
                    this.$window.history.back();
                }
            }, {
                key: "checkSessao",
                value: function checkSessao(secao) {
                    if (this.$state.current.data) {
                        this._secaoAtual = this.$state.current;
                    }
                    return secao.data === this._secaoAtual.data;
                }
            }, {
                key: "buildSecoes",
                value: function buildSecoes() {
                    var _this2 = this;

                    this.sessaoGroups = new Map();
                    var secoes = this.$state.get();
                    secoes.forEach(function (secao) {
                        var data = secao.data;
                        if (!data) {
                            return;
                        }
                        var isSection = secao.name && data && data.group;
                        if (!isSection) {
                            return;
                        }
                        _this2.sessaoGroups.set(data.group, _this2.sessaoGroups.get(data.group) || []);
                        _this2.sessaoGroups.get(data.group).push({
                            name: secao.name,
                            state: secao.name,
                            data: data,
                            initializing: false
                        });
                    });
                }
            }, {
                key: "ajuda",
                value: function ajuda() {
                    var ajudaDialog = this.$dialog.create({
                        title: 'Ajuda',
                        template: '<iframe class="googledocs-iframe" src="https://google.com"></iframe>',
                        modal: true,
                        width: 700,
                        height: 340
                    });
                    ajudaDialog.open();
                }
            }, {
                key: "secao",
                get: function get() {
                    return this._secaoAtual;
                },
                set: function set(v) {
                    this._secaoAtual = v;
                }
            }, {
                key: "sessaoGroups",
                get: function get() {
                    return this._sessaoGroups;
                },
                set: function set(v) {
                    this._sessaoGroups = v;
                }
            }]);

            return ModuleController;
        }(), _a.$inject = ['$state', '$scope', '$dialog', '$window'], _a);
        this.templateUrl = 'component/SidebarComponent.html';
    }

    _createClass(SidebarComponent, null, [{
        key: "factory",
        value: function factory() {
            return new SidebarComponent();
        }
    }]);

    return SidebarComponent;
}();

exports.SidebarComponent = SidebarComponent;

},{}],3:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", { value: true });

var CamelCaseToStrFilter = function () {
    function CamelCaseToStrFilter() {
        _classCallCheck(this, CamelCaseToStrFilter);
    }

    _createClass(CamelCaseToStrFilter, [{
        key: "filtro",
        value: function filtro() {
            return function (text) {
                var result = text.replace(/([A-Z])/g, ' $1');
                return result.charAt(0).toUpperCase() + result.slice(1);
            };
        }
    }]);

    return CamelCaseToStrFilter;
}();

exports.CamelCaseToStrFilter = CamelCaseToStrFilter;

},{}],4:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", { value: true });
var CamelCaseToStrFilter_1 = require("./CamelCaseToStrFilter");
var FromMap_1 = require("./FromMap");

var FilterInject = function () {
    function FilterInject() {
        _classCallCheck(this, FilterInject);
    }

    _createClass(FilterInject, null, [{
        key: "register",
        value: function register(app) {
            return app.filter('camelCase', new CamelCaseToStrFilter_1.CamelCaseToStrFilter().filtro).filter('fromMap', new FromMap_1.FromMap().filtro);
        }
    }]);

    return FilterInject;
}();

exports.FilterInject = FilterInject;

},{"./CamelCaseToStrFilter":3,"./FromMap":5}],5:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", { value: true });

var FromMap = function () {
    function FromMap() {
        _classCallCheck(this, FromMap);
    }

    _createClass(FromMap, [{
        key: "filtro",
        value: function filtro() {
            return function (input) {
                var out = {};
                input.forEach(function (v, k) {
                    return out[k] = v;
                });
                return out;
            };
        }
    }]);

    return FromMap;
}();

exports.FromMap = FromMap;

},{}],6:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var ViewInject_1 = require("./view/ViewInject");
var ComponentInject_1 = require("./component/ComponentInject");
var FilterInject_1 = require("./filter/FilterInject");
var ServiceInject_1 = require("./service/ServiceInject");
function load(window) {
    var angular = window.angular;
    if (!angular) {
        return;
    }
    var app = angular.module('app', ['syonet', 'syonetResource', 'ui.router', '$moduleWorkspace', 'syonet.namebeautifier', 'ui.utils', 'pascalprecht.translate']);
    FilterInject_1.FilterInject.register(app);
    ComponentInject_1.ComponentInject.register(app);
    ViewInject_1.ViewInject.register(app);
    ServiceInject_1.ServiceInject.register(app);
}
load(window);

},{"./component/ComponentInject":1,"./filter/FilterInject":4,"./service/ServiceInject":8,"./view/ViewInject":14}],7:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", { value: true });

var ParametroService = function () {
    function ParametroService($http) {
        _classCallCheck(this, ParametroService);

        this.$http = $http;
        this.url = '/api/parametro/';
    }

    _createClass(ParametroService, [{
        key: "list",
        value: function list() {
            return this.$http.get(this.url);
        }
    }]);

    return ParametroService;
}();

ParametroService.$inject = ['$http'];
exports.ParametroService = ParametroService;

},{}],8:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", { value: true });
var ParametroService_1 = require("./ParametroService");

var ServiceInject = function () {
    function ServiceInject() {
        _classCallCheck(this, ServiceInject);
    }

    _createClass(ServiceInject, null, [{
        key: "register",
        value: function register(app) {
            console.log(1);
            return app.service('parametroService', ParametroService_1.ParametroService);
        }
    }]);

    return ServiceInject;
}();

exports.ServiceInject = ServiceInject;

},{"./ParametroService":7}],9:[function(require,module,exports){
"use strict";

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Decorator para montar a config do state
 * @param prop
 */
function SyoView(prop, data) {
    return function (target) {
        if (!prop.controller) {
            prop.controller = target.name;
        }
        prop.data = data;
        exports.config.set(prop.state, prop);
        exports.config = new Map([].concat(_toConsumableArray(exports.config.entries())).sort());
    };
}
exports.SyoView = SyoView;
exports.config = new Map();

},{}],10:[function(require,module,exports){
"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", { value: true });

var Espanhol = function Espanhol() {
    _classCallCheck(this, Espanhol);

    this.TITULO = 'Auditoría';
    this.GRUPO = 'Actualización';
    this.MSG_NENHUM_LOG = 'Ola mundo';
};

exports.Espanhol = Espanhol;

},{}],11:[function(require,module,exports){
"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", { value: true });

var Portugues = function Portugues() {
    _classCallCheck(this, Portugues);

    this.MSG_NENHUM_LOG = 'Sei la';
    this.TITULO = 'Hello World!';
    this.GRUPO = 'Grupo';
};

exports.Portugues = Portugues;

},{}],12:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", { value: true });
var Portugues_1 = require("./Portugues");
var Espanhol_1 = require("./Espanhol");

var TraducaoFactory = function () {
    function TraducaoFactory() {
        _classCallCheck(this, TraducaoFactory);
    }

    _createClass(TraducaoFactory, [{
        key: "create",
        value: function create(idioma) {
            switch (idioma) {
                case 'PT-BR':
                    return new Portugues_1.Portugues();
                case 'ES':
                    return new Espanhol_1.Espanhol();
                default:
                    return new Portugues_1.Portugues();
            }
        }
    }]);

    return TraducaoFactory;
}();

exports.TraducaoFactory = TraducaoFactory;

},{"./Espanhol":10,"./Portugues":11}],13:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
    var c = arguments.length,
        r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
        d;
    if ((typeof Reflect === "undefined" ? "undefined" : _typeof(Reflect)) === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    }return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("../syonet/core");
var ArquivoController = function () {
    function ArquivoController($state, parametroService) {
        var _this = this;

        _classCallCheck(this, ArquivoController);

        this.$state = $state;
        this.parametroService = parametroService;
        this.title = $state.current.data.title;
        this.parametroService.list().then(function (response) {
            return _this.parametros = response.data;
        });
    }

    _createClass(ArquivoController, [{
        key: "parametros",
        get: function get() {
            return this._parametros;
        },
        set: function set(v) {
            this._parametros = v;
        }
    }, {
        key: "title",
        get: function get() {
            return this._title;
        },
        set: function set(v) {
            this._title = v;
        }
    }]);

    return ArquivoController;
}();
ArquivoController.$inject = ['$state', 'parametroService'];
ArquivoController = __decorate([core_1.SyoView({
    templateUrl: 'view/ArquivoController.html',
    state: 'historicoCliente',
    controllerAs: 'ArquivoCtrl',
    url: '/historico/cliente'
}, {
    group: 'Guardian',
    title: 'TITULO',
    icon: 'user'
})], ArquivoController);
exports.ArquivoController = ArquivoController;

},{"../syonet/core":9}],14:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", { value: true });
var ArquivoController_1 = require("./ArquivoController");
var core_1 = require("./../syonet/core");
var TraducaoFactory_1 = require("../translate/TraducaoFactory");

var ViewInject = function () {
    function ViewInject() {
        _classCallCheck(this, ViewInject);
    }

    _createClass(ViewInject, null, [{
        key: "register",
        value: function register(app) {
            return app.controller('ArquivoController', ArquivoController_1.ArquivoController).config(['$urlRouterProvider', '$stateProvider', '$translateProvider', function ($urlRouterProvider, $stateProvider, $translateProvider) {
                $translateProvider.useSanitizeValueStrategy('escape');
                $translateProvider.preferredLanguage('PT-BR');
                $translateProvider.useLoader('customLoader');
                core_1.config.forEach(function (value, key) {
                    $stateProvider.state(key, value);
                });
                $urlRouterProvider.otherwise('/historico/cliente');
            }]).factory('customLoader', function ($q, $http) {
                return function (options) {
                    var url = '/api/parametro/';
                    var promise = $http.get(url + 'IDIOMA_SISTEMA');
                    var dfd = $q.defer();
                    promise.then(function (response) {
                        dfd.resolve(new TraducaoFactory_1.TraducaoFactory().create(response.data.valor));
                    });
                    return dfd.promise;
                };
            });
        }
    }]);

    return ViewInject;
}();

exports.ViewInject = ViewInject;

},{"../translate/TraducaoFactory":12,"./../syonet/core":9,"./ArquivoController":13}]},{},[6])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvY29tcG9uZW50L0NvbXBvbmVudEluamVjdC50cyIsInNyYy9jb21wb25lbnQvU2lkZWJhckNvbXBvbmVudC50cyIsInNyYy9maWx0ZXIvQ2FtZWxDYXNlVG9TdHJGaWx0ZXIudHMiLCJzcmMvZmlsdGVyL0ZpbHRlckluamVjdC50cyIsInNyYy9maWx0ZXIvRnJvbU1hcC50cyIsInNyYy9tYWluLnRzIiwic3JjL3NlcnZpY2UvUGFyYW1ldHJvU2VydmljZS50cyIsInNyYy9zZXJ2aWNlL1NlcnZpY2VJbmplY3QudHMiLCJzcmMvc3lvbmV0L2NvcmUudHMiLCJzcmMvdHJhbnNsYXRlL0VzcGFuaG9sLnRzIiwic3JjL3RyYW5zbGF0ZS9Qb3J0dWd1ZXMudHMiLCJzcmMvdHJhbnNsYXRlL1RyYWR1Y2FvRmFjdG9yeS50cyIsInNyYy92aWV3L0FycXVpdm9Db250cm9sbGVyLnRzIiwic3JjL3ZpZXcvVmlld0luamVjdC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7QUNDQSxJQUFBLHFCQUFBLFFBQUEsb0JBQUEsQ0FBQTs7SUFFQSxlOzs7Ozs7O2lDQUN5QixHLEVBQWU7QUFDcEMsbUJBQU8sSUFBSSxTQUFKLENBQWUsU0FBZixFQUEwQixtQkFBQSxnQkFBQSxDQUFpQixPQUEzQyxDQUFQO0FBQ0Q7Ozs7OztBQUhILFFBQUEsZUFBQSxHQUFBLGVBQUE7Ozs7Ozs7Ozs7O0lDQ0EsZ0I7QUFBQSxnQ0FBQTtBQUFBOzs7QUFDUyxhQUFBLFFBQUEsR0FBbUIsR0FBbkI7QUFDQSxhQUFBLFlBQUEsR0FBdUIsWUFBdkI7QUFDQSxhQUFBLFVBQUEsSUFBVTtBQWtCZixzQ0FDVSxNQURWLEVBRVUsTUFGVixFQUdVLE9BSFYsRUFJVSxPQUpWLEVBSXlCO0FBQUE7O0FBQUE7O0FBSGYscUJBQUEsTUFBQSxHQUFBLE1BQUE7QUFDQSxxQkFBQSxNQUFBLEdBQUEsTUFBQTtBQUNBLHFCQUFBLE9BQUEsR0FBQSxPQUFBO0FBQ0EscUJBQUEsT0FBQSxHQUFBLE9BQUE7QUFuQkYscUJBQUEsV0FBQSxHQUFtQixFQUFuQjtBQW9CTixxQkFBSyxXQUFMO0FBQ0EscUJBQUssTUFBTCxDQUFZLEdBQVosQ0FBZ0IscUJBQWhCLEVBQXVDLFVBQUMsR0FBRCxFQUFXLE9BQVgsRUFBMkI7QUFDaEUsMEJBQUssWUFBTCxDQUFrQixPQUFsQixDQUEwQixVQUFDLEtBQUQsRUFBUSxHQUFSLEVBQWU7QUFDdkMsNEJBQU0sUUFBUSxNQUFNLElBQU4sQ0FBVyxVQUFDLEVBQUQ7QUFBQSxtQ0FBYSxHQUFHLElBQUgsS0FBWSxRQUFRLElBQWpDO0FBQUEseUJBQVgsQ0FBZDtBQUNBLDRCQUFJLEtBQUosRUFBVztBQUNULGtDQUFNLFlBQU4sR0FBcUIsS0FBckI7QUFDRDtBQUNGLHFCQUxEO0FBTUQsaUJBUEQ7QUFRRDs7QUFoQ2M7QUFBQTtBQUFBLDRDQWlDTjtBQUNQLHlCQUFLLE9BQUwsQ0FBYSxPQUFiLENBQXFCLElBQXJCO0FBQ0Q7QUFuQ2M7QUFBQTtBQUFBLDRDQW9DSCxLQXBDRyxFQW9DUztBQUN0Qix3QkFBSSxLQUFLLE1BQUwsQ0FBWSxPQUFaLENBQW9CLElBQXhCLEVBQThCO0FBQzVCLDZCQUFLLFdBQUwsR0FBbUIsS0FBSyxNQUFMLENBQVksT0FBL0I7QUFDRDtBQUNELDJCQUFPLE1BQU0sSUFBTixLQUFlLEtBQUssV0FBTCxDQUFpQixJQUF2QztBQUNEO0FBekNjO0FBQUE7QUFBQSw4Q0EyQ0o7QUFBQTs7QUFDVCx5QkFBSyxZQUFMLEdBQW9CLElBQUksR0FBSixFQUFwQjtBQUNBLHdCQUFNLFNBQW1CLEtBQUssTUFBTCxDQUFZLEdBQVosRUFBekI7QUFDQSwyQkFBTyxPQUFQLENBQWUsVUFBQyxLQUFELEVBQWtCO0FBQy9CLDRCQUFJLE9BQU8sTUFBTSxJQUFqQjtBQUNBLDRCQUFJLENBQUMsSUFBTCxFQUFXO0FBQ1Q7QUFDRDtBQUNELDRCQUFJLFlBQVksTUFBTSxJQUFOLElBQWMsSUFBZCxJQUFzQixLQUFLLEtBQTNDO0FBQ0EsNEJBQUksQ0FBQyxTQUFMLEVBQWdCO0FBQ2Q7QUFDRDtBQUNELCtCQUFLLFlBQUwsQ0FBa0IsR0FBbEIsQ0FBc0IsS0FBSyxLQUEzQixFQUFrQyxPQUFLLFlBQUwsQ0FBa0IsR0FBbEIsQ0FBc0IsS0FBSyxLQUEzQixLQUFxQyxFQUF2RTtBQUNBLCtCQUFLLFlBQUwsQ0FBa0IsR0FBbEIsQ0FBc0IsS0FBSyxLQUEzQixFQUFrQyxJQUFsQyxDQUF1QztBQUNyQyxrQ0FBTSxNQUFNLElBRHlCO0FBRXJDLG1DQUFPLE1BQU0sSUFGd0I7QUFHckMsa0NBQU0sSUFIK0I7QUFJckMsMENBQWM7QUFKdUIseUJBQXZDO0FBTUQscUJBaEJEO0FBaUJEO0FBL0RjO0FBQUE7QUFBQSx3Q0FpRVY7QUFDSCx3QkFBSSxjQUFjLEtBQUssT0FBTCxDQUFhLE1BQWIsQ0FBb0I7QUFDcEMsK0JBQU8sT0FENkI7QUFFcEMsa0NBQVUsc0VBRjBCO0FBR3BDLCtCQUFPLElBSDZCO0FBSXBDLCtCQUFPLEdBSjZCO0FBS3BDLGdDQUFRO0FBTDRCLHFCQUFwQixDQUFsQjtBQU9BLGdDQUFZLElBQVo7QUFDRDtBQTFFYztBQUFBO0FBQUEsb0NBSUM7QUFDZCwyQkFBTyxLQUFLLFdBQVo7QUFDRCxpQkFOYztBQUFBLGtDQU9FLENBUEYsRUFPUTtBQUNyQix5QkFBSyxXQUFMLEdBQW1CLENBQW5CO0FBQ0Q7QUFUYztBQUFBO0FBQUEsb0NBWVE7QUFDckIsMkJBQU8sS0FBSyxhQUFaO0FBQ0QsaUJBZGM7QUFBQSxrQ0FlUyxDQWZULEVBZThCO0FBQzNDLHlCQUFLLGFBQUwsR0FBcUIsQ0FBckI7QUFDRDtBQWpCYzs7QUFBQTtBQUFBLGFBQ0QsR0FBQSxPQUFBLEdBQW9CLENBQUMsUUFBRCxFQUFXLFFBQVgsRUFBcUIsU0FBckIsRUFBZ0MsU0FBaEMsQ0FEbkIsRUEyRWhCLEVBM0VNO0FBNkVBLGFBQUEsV0FBQSxHQUFzQixpQ0FBdEI7QUFJUjs7OztrQ0FIc0I7QUFDbkIsbUJBQU8sSUFBSSxnQkFBSixFQUFQO0FBQ0Q7Ozs7OztBQW5GSCxRQUFBLGdCQUFBLEdBQUEsZ0JBQUE7Ozs7Ozs7Ozs7O0lDSkEsb0I7Ozs7Ozs7aUNBQ2U7QUFDWCxtQkFBTyxVQUFDLElBQUQsRUFBbUI7QUFDeEIsb0JBQUksU0FBUyxLQUFLLE9BQUwsQ0FBYSxVQUFiLEVBQXlCLEtBQXpCLENBQWI7QUFDQSx1QkFBTyxPQUFPLE1BQVAsQ0FBYyxDQUFkLEVBQWlCLFdBQWpCLEtBQWlDLE9BQU8sS0FBUCxDQUFhLENBQWIsQ0FBeEM7QUFDRCxhQUhEO0FBSUQ7Ozs7OztBQU5ILFFBQUEsb0JBQUEsR0FBQSxvQkFBQTs7Ozs7Ozs7OztBQ0FBLElBQUEseUJBQUEsUUFBQSx3QkFBQSxDQUFBO0FBQ0EsSUFBQSxZQUFBLFFBQUEsV0FBQSxDQUFBOztJQUVBLFk7Ozs7Ozs7aUNBQ3lCLEcsRUFBZTtBQUNwQyxtQkFBTyxJQUNKLE1BREksQ0FDRyxXQURILEVBQ2dCLElBQUksdUJBQUEsb0JBQUosR0FBMkIsTUFEM0MsRUFFSixNQUZJLENBRUcsU0FGSCxFQUVjLElBQUksVUFBQSxPQUFKLEdBQWMsTUFGNUIsQ0FBUDtBQUdEOzs7Ozs7QUFMSCxRQUFBLFlBQUEsR0FBQSxZQUFBOzs7Ozs7Ozs7OztJQ0hBLE87Ozs7Ozs7aUNBQ2U7QUFDWCxtQkFBTyxVQUFDLEtBQUQsRUFBOEI7QUFDbkMsb0JBQUksTUFBVyxFQUFmO0FBQ0Esc0JBQU0sT0FBTixDQUFjLFVBQUMsQ0FBRCxFQUFJLENBQUo7QUFBQSwyQkFBVSxJQUFJLENBQUosSUFBUyxDQUFuQjtBQUFBLGlCQUFkO0FBQ0EsdUJBQU8sR0FBUDtBQUNELGFBSkQ7QUFLRDs7Ozs7O0FBUEgsUUFBQSxPQUFBLEdBQUEsT0FBQTs7Ozs7O0FDQUEsSUFBQSxlQUFBLFFBQUEsbUJBQUEsQ0FBQTtBQUNBLElBQUEsb0JBQUEsUUFBQSw2QkFBQSxDQUFBO0FBQ0EsSUFBQSxpQkFBQSxRQUFBLHVCQUFBLENBQUE7QUFDQSxJQUFBLGtCQUFBLFFBQUEseUJBQUEsQ0FBQTtBQUVBLFNBQUEsSUFBQSxDQUFnQixNQUFoQixFQUEyQjtBQUN6QixRQUFJLFVBQVUsT0FBTyxPQUFyQjtBQUNBLFFBQUssQ0FBQyxPQUFOLEVBQWdCO0FBQ2Q7QUFDRDtBQUNELFFBQUksTUFBa0IsUUFBUSxNQUFSLENBQWUsS0FBZixFQUFzQixDQUMxQyxRQUQwQyxFQUUxQyxnQkFGMEMsRUFHMUMsV0FIMEMsRUFJMUMsa0JBSjBDLEVBSzFDLHVCQUwwQyxFQU0xQyxVQU4wQyxFQU8xQyx3QkFQMEMsQ0FBdEIsQ0FBdEI7QUFTQSxtQkFBQSxZQUFBLENBQWEsUUFBYixDQUF1QixHQUF2QjtBQUNBLHNCQUFBLGVBQUEsQ0FBZ0IsUUFBaEIsQ0FBMEIsR0FBMUI7QUFDQSxpQkFBQSxVQUFBLENBQVcsUUFBWCxDQUFxQixHQUFyQjtBQUNBLG9CQUFBLGFBQUEsQ0FBYyxRQUFkLENBQXdCLEdBQXhCO0FBQ0Q7QUFFRCxLQUFNLE1BQU47Ozs7Ozs7Ozs7O0lDdkJBLGdCO0FBR0ksOEJBQW9CLEtBQXBCLEVBQTBDO0FBQUE7O0FBQXRCLGFBQUEsS0FBQSxHQUFBLEtBQUE7QUFEWixhQUFBLEdBQUEsR0FBYyxpQkFBZDtBQUN1Qzs7OzsrQkFFM0M7QUFDRixtQkFBTyxLQUFLLEtBQUwsQ0FBVyxHQUFYLENBQWUsS0FBSyxHQUFwQixDQUFQO0FBQ0Q7Ozs7OztBQU5NLGlCQUFBLE9BQUEsR0FBb0IsQ0FBQyxPQUFELENBQXBCO0FBRFgsUUFBQSxnQkFBQSxHQUFBLGdCQUFBOzs7Ozs7Ozs7O0FDRkEsSUFBQSxxQkFBQSxRQUFBLG9CQUFBLENBQUE7O0lBRUEsYTs7Ozs7OztpQ0FDeUIsRyxFQUFlO0FBQ3BDLG9CQUFRLEdBQVIsQ0FBWSxDQUFaO0FBQ0EsbUJBQU8sSUFDSixPQURJLENBQ0ksa0JBREosRUFDd0IsbUJBQUEsZ0JBRHhCLENBQVA7QUFFRDs7Ozs7O0FBTEgsUUFBQSxhQUFBLEdBQUEsYUFBQTs7Ozs7Ozs7QUNEQTs7OztBQUlBLFNBQUEsT0FBQSxDQUF3QixJQUF4QixFQUFxQyxJQUFyQyxFQUFnRDtBQUM5QyxXQUFPLFVBQVUsTUFBVixFQUFxRTtBQUMxRSxZQUFJLENBQUMsS0FBSyxVQUFWLEVBQXNCO0FBQ3BCLGlCQUFLLFVBQUwsR0FBa0IsT0FBTyxJQUF6QjtBQUNEO0FBQ0QsYUFBSyxJQUFMLEdBQVksSUFBWjtBQUNBLGdCQUFBLE1BQUEsQ0FBTyxHQUFQLENBQVcsS0FBSyxLQUFoQixFQUF1QixJQUF2QjtBQUNBLGdCQUFBLE1BQUEsR0FBUyxJQUFJLEdBQUosQ0FBUSw2QkFBSSxRQUFBLE1BQUEsQ0FBTyxPQUFQLEVBQUosR0FBc0IsSUFBdEIsRUFBUixDQUFUO0FBQ0QsS0FQRDtBQVFEO0FBVEQsUUFBQSxPQUFBLEdBQUEsT0FBQTtBQTRCVyxRQUFBLE1BQUEsR0FBNEIsSUFBSSxHQUFKLEVBQTVCOzs7Ozs7Ozs7SUMvQlgsUSxHQUFBLG9CQUFBO0FBQUE7O0FBQ0ksU0FBQSxNQUFBLEdBQVMsV0FBVDtBQUNBLFNBQUEsS0FBQSxHQUFRLGVBQVI7QUFDQSxTQUFBLGNBQUEsR0FBaUIsV0FBakI7QUFDSCxDOztBQUpELFFBQUEsUUFBQSxHQUFBLFFBQUE7Ozs7Ozs7OztJQ0FBLFMsR0FBQSxxQkFBQTtBQUFBOztBQUNJLFNBQUEsY0FBQSxHQUFpQixRQUFqQjtBQUNBLFNBQUEsTUFBQSxHQUFTLGNBQVQ7QUFDQSxTQUFBLEtBQUEsR0FBUSxPQUFSO0FBQ0YsQzs7QUFKRixRQUFBLFNBQUEsR0FBQSxTQUFBOzs7Ozs7Ozs7O0FDQUEsSUFBQSxjQUFBLFFBQUEsYUFBQSxDQUFBO0FBQ0EsSUFBQSxhQUFBLFFBQUEsWUFBQSxDQUFBOztJQUVBLGU7Ozs7Ozs7K0JBQ1csTSxFQUFjO0FBQ2pCLG9CQUFRLE1BQVI7QUFDSSxxQkFBSyxPQUFMO0FBQ0ksMkJBQU8sSUFBSSxZQUFBLFNBQUosRUFBUDtBQUNKLHFCQUFLLElBQUw7QUFDSSwyQkFBTyxJQUFJLFdBQUEsUUFBSixFQUFQO0FBQ0o7QUFDSSwyQkFBTyxJQUFJLFlBQUEsU0FBSixFQUFQO0FBTlI7QUFRSDs7Ozs7O0FBVkwsUUFBQSxlQUFBLEdBQUEsZUFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNMQSxJQUFBLFNBQUEsUUFBQSxnQkFBQSxDQUFBO0FBZ0JBLElBQUE7QUFvQkUsK0JBQ1UsTUFEVixFQUVVLGdCQUZWLEVBRTRDO0FBQUE7O0FBQUE7O0FBRGxDLGFBQUEsTUFBQSxHQUFBLE1BQUE7QUFDQSxhQUFBLGdCQUFBLEdBQUEsZ0JBQUE7QUFFUixhQUFLLEtBQUwsR0FBYSxPQUFPLE9BQVAsQ0FBZSxJQUFmLENBQW9CLEtBQWpDO0FBQ0EsYUFBSyxnQkFBTCxDQUFzQixJQUF0QixHQUE2QixJQUE3QixDQUFtQztBQUFBLG1CQUFZLE1BQUssVUFBTCxHQUFrQixTQUFTLElBQXZDO0FBQUEsU0FBbkM7QUFDRDs7QUExQkg7QUFBQTtBQUFBLDRCQVF1QjtBQUNuQixtQkFBTyxLQUFLLFdBQVo7QUFDRCxTQVZIO0FBQUEsMEJBV3dCLENBWHhCLEVBVzZDO0FBQ3pDLGlCQUFLLFdBQUwsR0FBbUIsQ0FBbkI7QUFDRDtBQWJIO0FBQUE7QUFBQSw0QkFja0I7QUFDZCxtQkFBTyxLQUFLLE1BQVo7QUFDRCxTQWhCSDtBQUFBLDBCQWlCbUIsQ0FqQm5CLEVBaUI0QjtBQUN4QixpQkFBSyxNQUFMLEdBQWMsQ0FBZDtBQUNEO0FBbkJIOztBQUFBO0FBQUEsR0FBQTtBQUNnQixrQkFBQSxPQUFBLEdBQW9CLENBQ2hDLFFBRGdDLEVBRWhDLGtCQUZnQyxDQUFwQjtBQURILG9CQUFpQixXQUFBLENBWDdCLE9BQUEsT0FBQSxDQUFRO0FBQ1AsaUJBQWEsNkJBRE47QUFFUCxXQUFPLGtCQUZBO0FBR1Asa0JBQWMsYUFIUDtBQUlQLFNBQUs7QUFKRSxDQUFSLEVBS0U7QUFDRCxXQUFPLFVBRE47QUFFRCxXQUFPLFFBRk47QUFHRCxVQUFNO0FBSEwsQ0FMRixDQVc2QixDQUFBLEVBQWpCLGlCQUFpQixDQUFqQjtBQUFBLFFBQUEsaUJBQUEsR0FBQSxpQkFBQTs7Ozs7Ozs7OztBQ2hCYixJQUFBLHNCQUFBLFFBQUEscUJBQUEsQ0FBQTtBQUNBLElBQUEsU0FBQSxRQUFBLGtCQUFBLENBQUE7QUFFQSxJQUFBLG9CQUFBLFFBQUEsOEJBQUEsQ0FBQTs7SUFFQSxVOzs7Ozs7O2lDQUN5QixHLEVBQWU7QUFDcEMsbUJBQU8sSUFDSixVQURJLENBQ08sbUJBRFAsRUFDNEIsb0JBQUEsaUJBRDVCLEVBRUosTUFGSSxDQUVHLENBQUMsb0JBQUQsRUFBdUIsZ0JBQXZCLEVBQXlDLG9CQUF6QyxFQUE4RCxVQUNwRSxrQkFEb0UsRUFFcEUsY0FGb0UsRUFHcEUsa0JBSG9FLEVBR1A7QUFDN0QsbUNBQW1CLHdCQUFuQixDQUE0QyxRQUE1QztBQUNBLG1DQUFtQixpQkFBbkIsQ0FBcUMsT0FBckM7QUFDQSxtQ0FBbUIsU0FBbkIsQ0FBNkIsY0FBN0I7QUFDQSx1QkFBQSxNQUFBLENBQU8sT0FBUCxDQUFlLFVBQUMsS0FBRCxFQUFlLEdBQWYsRUFBOEI7QUFDM0MsbUNBQWUsS0FBZixDQUFxQixHQUFyQixFQUEwQixLQUExQjtBQUNELGlCQUZEO0FBR0EsbUNBQW1CLFNBQW5CLENBQThCLG9CQUE5QjtBQUNELGFBWE8sQ0FGSCxFQWNKLE9BZEksQ0FjSSxjQWRKLEVBY29CLFVBQUMsRUFBRCxFQUF3QixLQUF4QixFQUF1RDtBQUM5RSx1QkFBTyxVQUFDLE9BQUQsRUFBaUI7QUFDdEIsd0JBQU0sTUFBYyxpQkFBcEI7QUFDQSx3QkFBTSxVQUFVLE1BQU0sR0FBTixDQUFVLE1BQU0sZ0JBQWhCLENBQWhCO0FBQ0Esd0JBQUksTUFBOEIsR0FBRyxLQUFILEVBQWxDO0FBQ0EsNEJBQVEsSUFBUixDQUFhLFVBQUMsUUFBRCxFQUFrQjtBQUM3Qiw0QkFBSSxPQUFKLENBQVksSUFBSSxrQkFBQSxlQUFKLEdBQXNCLE1BQXRCLENBQTZCLFNBQVMsSUFBVCxDQUFjLEtBQTNDLENBQVo7QUFDRCxxQkFGRDtBQUdBLDJCQUFPLElBQUksT0FBWDtBQUNELGlCQVJEO0FBU0QsYUF4QkksQ0FBUDtBQXlCRDs7Ozs7O0FBM0JILFFBQUEsVUFBQSxHQUFBLFVBQUEiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbigpe2Z1bmN0aW9uIHIoZSxuLHQpe2Z1bmN0aW9uIG8oaSxmKXtpZighbltpXSl7aWYoIWVbaV0pe3ZhciBjPVwiZnVuY3Rpb25cIj09dHlwZW9mIHJlcXVpcmUmJnJlcXVpcmU7aWYoIWYmJmMpcmV0dXJuIGMoaSwhMCk7aWYodSlyZXR1cm4gdShpLCEwKTt2YXIgYT1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK2krXCInXCIpO3Rocm93IGEuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixhfXZhciBwPW5baV09e2V4cG9ydHM6e319O2VbaV1bMF0uY2FsbChwLmV4cG9ydHMsZnVuY3Rpb24ocil7dmFyIG49ZVtpXVsxXVtyXTtyZXR1cm4gbyhufHxyKX0scCxwLmV4cG9ydHMscixlLG4sdCl9cmV0dXJuIG5baV0uZXhwb3J0c31mb3IodmFyIHU9XCJmdW5jdGlvblwiPT10eXBlb2YgcmVxdWlyZSYmcmVxdWlyZSxpPTA7aTx0Lmxlbmd0aDtpKyspbyh0W2ldKTtyZXR1cm4gb31yZXR1cm4gcn0pKCkiLCJcbmltcG9ydCB7IFNpZGViYXJDb21wb25lbnQgfSBmcm9tICcuL1NpZGViYXJDb21wb25lbnQnXG5cbmV4cG9ydCBjbGFzcyBDb21wb25lbnRJbmplY3Qge1xuICBwdWJsaWMgc3RhdGljIHJlZ2lzdGVyKGFwcDogbmcuSU1vZHVsZSk6IG5nLklNb2R1bGUge1xuICAgIHJldHVybiBhcHAuZGlyZWN0aXZlKCAnc2lkZWJhcicsIFNpZGViYXJDb21wb25lbnQuZmFjdG9yeSApXG4gIH1cbn1cbiIsImltcG9ydCB7IElTdGF0ZVNlcnZpY2UsIElTdGF0ZSB9IGZyb20gJ2FuZ3VsYXItdWktcm91dGVyJ1xuaW1wb3J0IHsgSVNjb3BlIH0gZnJvbSAnYW5ndWxhcidcbmltcG9ydCB7IFN0YXRlIH0gZnJvbSAnLi4vc3lvbmV0L2NvcmUnXG5cbmV4cG9ydCBjbGFzcyBTaWRlYmFyQ29tcG9uZW50IGltcGxlbWVudHMgbmcuSURpcmVjdGl2ZSB7XG4gIHB1YmxpYyByZXN0cmljdDogc3RyaW5nID0gJ0UnXG4gIHB1YmxpYyBjb250cm9sbGVyQXM6IHN0cmluZyA9ICdNb2R1bGVDdHJsJ1xuICBwdWJsaWMgY29udHJvbGxlciA9IGNsYXNzIE1vZHVsZUNvbnRyb2xsZXIgaW1wbGVtZW50cyBuZy5JQ29udHJvbGxlciB7XG4gICAgcHVibGljIHN0YXRpYyAkaW5qZWN0OiBzdHJpbmdbXSA9IFsnJHN0YXRlJywgJyRzY29wZScsICckZGlhbG9nJywgJyR3aW5kb3cnXVxuXG4gICAgcHJpdmF0ZSBfc2VjYW9BdHVhbDogYW55ID0ge31cbiAgICBwdWJsaWMgZ2V0IHNlY2FvKCk6IGFueSB7XG4gICAgICByZXR1cm4gdGhpcy5fc2VjYW9BdHVhbFxuICAgIH1cbiAgICBwdWJsaWMgc2V0IHNlY2FvKHY6IGFueSkge1xuICAgICAgdGhpcy5fc2VjYW9BdHVhbCA9IHZcbiAgICB9XG5cbiAgICBwcml2YXRlIF9zZXNzYW9Hcm91cHM6IE1hcDxzdHJpbmcsIGFueVtdPlxuICAgIHB1YmxpYyBnZXQgc2Vzc2FvR3JvdXBzKCk6IE1hcDxzdHJpbmcsIGFueVtdPiB7XG4gICAgICByZXR1cm4gdGhpcy5fc2Vzc2FvR3JvdXBzXG4gICAgfVxuICAgIHB1YmxpYyBzZXQgc2Vzc2FvR3JvdXBzKHY6IE1hcDxzdHJpbmcsIGFueVtdPikge1xuICAgICAgdGhpcy5fc2Vzc2FvR3JvdXBzID0gdlxuICAgIH1cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgIHByaXZhdGUgJHN0YXRlOiBJU3RhdGVTZXJ2aWNlLFxuICAgICAgcHJpdmF0ZSAkc2NvcGU6IElTY29wZSxcbiAgICAgIHByaXZhdGUgJGRpYWxvZzogYW55LFxuICAgICAgcHJpdmF0ZSAkd2luZG93OiBXaW5kb3cpIHtcbiAgICAgIHRoaXMuYnVpbGRTZWNvZXMoKVxuICAgICAgdGhpcy4kc2NvcGUuJG9uKCckc3RhdGVDaGFuZ2VTdWNjZXNzJywgKGV2dDogYW55LCB0b1N0YXRlOiBhbnkpID0+IHtcbiAgICAgICAgdGhpcy5zZXNzYW9Hcm91cHMuZm9yRWFjaCgoZ3JvdXAsIGtleSkgPT4ge1xuICAgICAgICAgIGNvbnN0IHNlY2FvID0gZ3JvdXAuZmluZCgoZWw6IGFueSkgPT4gZWwubmFtZSA9PT0gdG9TdGF0ZS5uYW1lKVxuICAgICAgICAgIGlmIChzZWNhbykge1xuICAgICAgICAgICAgc2VjYW8uaW5pdGlhbGl6aW5nID0gZmFsc2VcbiAgICAgICAgICB9XG4gICAgICAgIH0pXG4gICAgICB9KVxuICAgIH1cbiAgICBiYWNrU3RhdGUoKSB7XG4gICAgICB0aGlzLiR3aW5kb3cuaGlzdG9yeS5iYWNrKClcbiAgICB9XG4gICAgY2hlY2tTZXNzYW8oc2VjYW86IFN0YXRlKTogYm9vbGVhbiB7XG4gICAgICBpZiAodGhpcy4kc3RhdGUuY3VycmVudC5kYXRhKSB7XG4gICAgICAgIHRoaXMuX3NlY2FvQXR1YWwgPSB0aGlzLiRzdGF0ZS5jdXJyZW50XG4gICAgICB9XG4gICAgICByZXR1cm4gc2VjYW8uZGF0YSA9PT0gdGhpcy5fc2VjYW9BdHVhbC5kYXRhXG4gICAgfVxuXG4gICAgYnVpbGRTZWNvZXMoKSB7XG4gICAgICB0aGlzLnNlc3Nhb0dyb3VwcyA9IG5ldyBNYXA8c3RyaW5nLCBhbnlbXT4oKVxuICAgICAgY29uc3Qgc2Vjb2VzOiBJU3RhdGVbXSA9IHRoaXMuJHN0YXRlLmdldCgpXG4gICAgICBzZWNvZXMuZm9yRWFjaCgoc2VjYW86IElTdGF0ZSkgPT4ge1xuICAgICAgICBsZXQgZGF0YSA9IHNlY2FvLmRhdGFcbiAgICAgICAgaWYgKCFkYXRhKSB7XG4gICAgICAgICAgcmV0dXJuXG4gICAgICAgIH1cbiAgICAgICAgbGV0IGlzU2VjdGlvbiA9IHNlY2FvLm5hbWUgJiYgZGF0YSAmJiBkYXRhLmdyb3VwXG4gICAgICAgIGlmICghaXNTZWN0aW9uKSB7XG4gICAgICAgICAgcmV0dXJuXG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5zZXNzYW9Hcm91cHMuc2V0KGRhdGEuZ3JvdXAsIHRoaXMuc2Vzc2FvR3JvdXBzLmdldChkYXRhLmdyb3VwKSB8fCBbXSlcbiAgICAgICAgdGhpcy5zZXNzYW9Hcm91cHMuZ2V0KGRhdGEuZ3JvdXApLnB1c2goe1xuICAgICAgICAgIG5hbWU6IHNlY2FvLm5hbWUsXG4gICAgICAgICAgc3RhdGU6IHNlY2FvLm5hbWUsXG4gICAgICAgICAgZGF0YTogZGF0YSxcbiAgICAgICAgICBpbml0aWFsaXppbmc6IGZhbHNlXG4gICAgICAgIH0pXG4gICAgICB9KVxuICAgIH1cblxuICAgIGFqdWRhKCkge1xuICAgICAgbGV0IGFqdWRhRGlhbG9nID0gdGhpcy4kZGlhbG9nLmNyZWF0ZSh7XG4gICAgICAgIHRpdGxlOiAnQWp1ZGEnLFxuICAgICAgICB0ZW1wbGF0ZTogJzxpZnJhbWUgY2xhc3M9XCJnb29nbGVkb2NzLWlmcmFtZVwiIHNyYz1cImh0dHBzOi8vZ29vZ2xlLmNvbVwiPjwvaWZyYW1lPicsXG4gICAgICAgIG1vZGFsOiB0cnVlLFxuICAgICAgICB3aWR0aDogNzAwLFxuICAgICAgICBoZWlnaHQ6IDM0MFxuICAgICAgfSlcbiAgICAgIGFqdWRhRGlhbG9nLm9wZW4oKVxuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyB0ZW1wbGF0ZVVybDogc3RyaW5nID0gJ2NvbXBvbmVudC9TaWRlYmFyQ29tcG9uZW50Lmh0bWwnXG4gIHB1YmxpYyBzdGF0aWMgZmFjdG9yeSgpOiBuZy5JRGlyZWN0aXZlIHtcbiAgICByZXR1cm4gbmV3IFNpZGViYXJDb21wb25lbnQoKVxuICB9XG59XG4iLCJleHBvcnQgY2xhc3MgQ2FtZWxDYXNlVG9TdHJGaWx0ZXIge1xuICBwdWJsaWMgZmlsdHJvKCkge1xuICAgIHJldHVybiAodGV4dDogYW55KTogYW55ID0+IHtcbiAgICAgIGxldCByZXN1bHQgPSB0ZXh0LnJlcGxhY2UoLyhbQS1aXSkvZywgJyAkMScpXG4gICAgICByZXR1cm4gcmVzdWx0LmNoYXJBdCgwKS50b1VwcGVyQ2FzZSgpICsgcmVzdWx0LnNsaWNlKDEpXG4gICAgfVxuICB9XG59XG4iLCJpbXBvcnQgeyBDYW1lbENhc2VUb1N0ckZpbHRlciB9IGZyb20gJy4vQ2FtZWxDYXNlVG9TdHJGaWx0ZXInXG5pbXBvcnQgeyBGcm9tTWFwIH0gZnJvbSAnLi9Gcm9tTWFwJ1xuXG5leHBvcnQgY2xhc3MgRmlsdGVySW5qZWN0IHtcbiAgcHVibGljIHN0YXRpYyByZWdpc3RlcihhcHA6IG5nLklNb2R1bGUpOiBuZy5JTW9kdWxlIHtcbiAgICByZXR1cm4gYXBwXG4gICAgICAuZmlsdGVyKCdjYW1lbENhc2UnLCBuZXcgQ2FtZWxDYXNlVG9TdHJGaWx0ZXIoKS5maWx0cm8pXG4gICAgICAuZmlsdGVyKCdmcm9tTWFwJywgbmV3IEZyb21NYXAoKS5maWx0cm8pXG4gIH1cbn1cbiIsImV4cG9ydCBjbGFzcyBGcm9tTWFwIHtcbiAgcHVibGljIGZpbHRybygpIHtcbiAgICByZXR1cm4gKGlucHV0OiBNYXA8YW55LCBhbnk+KTogYW55ID0+IHtcbiAgICAgIGxldCBvdXQ6IGFueSA9IHt9XG4gICAgICBpbnB1dC5mb3JFYWNoKCh2LCBrKSA9PiBvdXRba10gPSB2KVxuICAgICAgcmV0dXJuIG91dFxuICAgIH1cbiAgfVxufVxuIiwiaW1wb3J0IHsgVmlld0luamVjdCB9IGZyb20gJy4vdmlldy9WaWV3SW5qZWN0J1xuaW1wb3J0IHsgQ29tcG9uZW50SW5qZWN0IH0gIGZyb20gJy4vY29tcG9uZW50L0NvbXBvbmVudEluamVjdCdcbmltcG9ydCB7IEZpbHRlckluamVjdCB9IGZyb20gJy4vZmlsdGVyL0ZpbHRlckluamVjdCdcbmltcG9ydCB7IFNlcnZpY2VJbmplY3QgfSBmcm9tICcuL3NlcnZpY2UvU2VydmljZUluamVjdCc7XG5cbmZ1bmN0aW9uIGxvYWQgKCB3aW5kb3c6IGFueSApIHtcbiAgbGV0IGFuZ3VsYXIgPSB3aW5kb3cuYW5ndWxhciBcbiAgaWYgKCAhYW5ndWxhciApIHtcbiAgICByZXR1cm5cbiAgfVxuICBsZXQgYXBwOiBuZy5JTW9kdWxlID0gYW5ndWxhci5tb2R1bGUoJ2FwcCcsIFtcbiAgICAnc3lvbmV0JyxcbiAgICAnc3lvbmV0UmVzb3VyY2UnLFxuICAgICd1aS5yb3V0ZXInLFxuICAgICckbW9kdWxlV29ya3NwYWNlJyxcbiAgICAnc3lvbmV0Lm5hbWViZWF1dGlmaWVyJyxcbiAgICAndWkudXRpbHMnLFxuICAgICdwYXNjYWxwcmVjaHQudHJhbnNsYXRlJ1xuICBdKVxuICBGaWx0ZXJJbmplY3QucmVnaXN0ZXIoIGFwcCApXG4gIENvbXBvbmVudEluamVjdC5yZWdpc3RlciggYXBwIClcbiAgVmlld0luamVjdC5yZWdpc3RlciggYXBwIClcbiAgU2VydmljZUluamVjdC5yZWdpc3RlciggYXBwIClcbn1cblxubG9hZCggd2luZG93IClcbiIsImltcG9ydCB7IElQYXJhbWV0cm8gfSBmcm9tIFwiLi4vdmFsdWUvSVBhcmFtZXRyb1wiO1xuXG5leHBvcnQgY2xhc3MgUGFyYW1ldHJvU2VydmljZSB7XG4gICAgc3RhdGljICRpbmplY3Q6IHN0cmluZ1tdID0gWyckaHR0cCddO1xuICAgIHByaXZhdGUgdXJsOiBzdHJpbmcgPSAnL2FwaS9wYXJhbWV0cm8vJztcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlICRodHRwOiBuZy5JSHR0cFNlcnZpY2UpIHsgfVxuICBcbiAgICBsaXN0KCk6IG5nLklIdHRwUHJvbWlzZTxBcnJheTxJUGFyYW1ldHJvPj4ge1xuICAgICAgcmV0dXJuIHRoaXMuJGh0dHAuZ2V0KHRoaXMudXJsKTtcbiAgICB9XG59IiwiaW1wb3J0IHsgUGFyYW1ldHJvU2VydmljZSB9IGZyb20gJy4vUGFyYW1ldHJvU2VydmljZSc7XG5cbmV4cG9ydCBjbGFzcyBTZXJ2aWNlSW5qZWN0IHtcbiAgcHVibGljIHN0YXRpYyByZWdpc3RlcihhcHA6IG5nLklNb2R1bGUpOiBuZy5JTW9kdWxlIHtcbiAgICBjb25zb2xlLmxvZygxKVxuICAgIHJldHVybiBhcHBcbiAgICAgIC5zZXJ2aWNlKCdwYXJhbWV0cm9TZXJ2aWNlJywgUGFyYW1ldHJvU2VydmljZSlcbiAgfVxufVxuIiwiXG4vKipcbiAqIERlY29yYXRvciBwYXJhIG1vbnRhciBhIGNvbmZpZyBkbyBzdGF0ZVxuICogQHBhcmFtIHByb3AgXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBTeW9WaWV3KHByb3A6IFN0YXRlLCBkYXRhPzogRGF0YSkge1xuICByZXR1cm4gZnVuY3Rpb24gKHRhcmdldDogbmcuSW5qZWN0YWJsZTxuZy5JQ29udHJvbGxlckNvbnN0cnVjdG9yPiAmIEZ1bmN0aW9uICkge1xuICAgIGlmICghcHJvcC5jb250cm9sbGVyKSB7XG4gICAgICBwcm9wLmNvbnRyb2xsZXIgPSB0YXJnZXQubmFtZVxuICAgIH1cbiAgICBwcm9wLmRhdGEgPSBkYXRhXG4gICAgY29uZmlnLnNldChwcm9wLnN0YXRlLCBwcm9wKVxuICAgIGNvbmZpZyA9IG5ldyBNYXAoWy4uLmNvbmZpZy5lbnRyaWVzKCldLnNvcnQoKSlcbiAgfVxufVxuXG5leHBvcnQgaW50ZXJmYWNlIFN0YXRlIHtcbiAgY29udHJvbGxlcj86IHN0cmluZyxcbiAgY29udHJvbGxlckFzPzogc3RyaW5nLFxuICB0ZW1wbGF0ZVVybDogc3RyaW5nLFxuICBzdGF0ZTogc3RyaW5nLFxuICBuYW1lPzogc3RyaW5nLFxuICB1cmw6IHN0cmluZyxcbiAgZGF0YT86IERhdGFcbiAgaW5pdGlhbGl6aW5nPzogYm9vbGVhblxufVxuXG5leHBvcnQgaW50ZXJmYWNlIERhdGEge1xuICBncm91cDogc3RyaW5nLFxuICB0aXRsZTogc3RyaW5nLFxuICBpY29uOiBzdHJpbmcsXG59XG5cbmV4cG9ydCBsZXQgY29uZmlnOiBNYXA8c3RyaW5nLFN0YXRlPiA9IG5ldyBNYXAoKVxuIiwiaW1wb3J0IHsgSVRyYWR1Y2FvIH0gZnJvbSAnLi9JVHJhZHVjYW8nXG5cbmV4cG9ydCBjbGFzcyBFc3BhbmhvbCBpbXBsZW1lbnRzIElUcmFkdWNhbyB7XG4gICAgVElUVUxPID0gJ0F1ZGl0b3LDrWEnXG4gICAgR1JVUE8gPSAnQWN0dWFsaXphY2nDs24nXG4gICAgTVNHX05FTkhVTV9MT0cgPSAnT2xhIG11bmRvJ1xufVxuIiwiaW1wb3J0IHsgSVRyYWR1Y2FvIH0gZnJvbSAnLi9JVHJhZHVjYW8nXG5cbmV4cG9ydCBjbGFzcyBQb3J0dWd1ZXMgaW1wbGVtZW50cyBJVHJhZHVjYW8ge1xuICAgIE1TR19ORU5IVU1fTE9HID0gJ1NlaSBsYSc7XG4gICAgVElUVUxPID0gJ0hlbGxvIFdvcmxkISdcbiAgICBHUlVQTyA9ICdHcnVwbydcbiB9IiwiaW1wb3J0IHsgY3JlYXRlIH0gZnJvbSAnZG9tYWluJ1xuaW1wb3J0IHsgSVRyYWR1Y2FvIH0gZnJvbSAnLi9JVHJhZHVjYW8nXG5pbXBvcnQgeyBQb3J0dWd1ZXMgfSBmcm9tICcuL1BvcnR1Z3VlcydcbmltcG9ydCB7IEVzcGFuaG9sIH0gZnJvbSAnLi9Fc3BhbmhvbCdcblxuZXhwb3J0IGNsYXNzIFRyYWR1Y2FvRmFjdG9yeSB7XG4gICAgY3JlYXRlKGlkaW9tYTogc3RyaW5nKTogSVRyYWR1Y2FvIHtcbiAgICAgICAgc3dpdGNoIChpZGlvbWEpIHtcbiAgICAgICAgICAgIGNhc2UgJ1BULUJSJzpcbiAgICAgICAgICAgICAgICByZXR1cm4gbmV3IFBvcnR1Z3VlcygpXG4gICAgICAgICAgICBjYXNlICdFUyc6XG4gICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBFc3BhbmhvbCgpXG4gICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgIHJldHVybiBuZXcgUG9ydHVndWVzKClcbiAgICAgICAgfVxuICAgIH1cbn1cbiIsImltcG9ydCB7IFN5b1ZpZXcsIFN0YXRlLCBEYXRhIH0gZnJvbSAnLi4vc3lvbmV0L2NvcmUnXG5pbXBvcnQgeyBJU3RhdGVTZXJ2aWNlIH0gZnJvbSAnYW5ndWxhci11aS1yb3V0ZXInXG5pbXBvcnQgeyBQYXJhbWV0cm9TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZS9QYXJhbWV0cm9TZXJ2aWNlJ1xuaW1wb3J0IHsgSVBhcmFtZXRybyB9IGZyb20gJy4uL3ZhbHVlL0lQYXJhbWV0cm8nXG5cbkBTeW9WaWV3KHsgXG4gIHRlbXBsYXRlVXJsOiAndmlldy9BcnF1aXZvQ29udHJvbGxlci5odG1sJywgXG4gIHN0YXRlOiAnaGlzdG9yaWNvQ2xpZW50ZScsXG4gIGNvbnRyb2xsZXJBczogJ0FycXVpdm9DdHJsJyxcbiAgdXJsOiAnL2hpc3Rvcmljby9jbGllbnRlJyBcbn0sIHtcbiAgZ3JvdXA6ICdHdWFyZGlhbicsXG4gIHRpdGxlOiAnVElUVUxPJyxcbiAgaWNvbjogJ3VzZXInXG59KVxuXG5leHBvcnQgY2xhc3MgQXJxdWl2b0NvbnRyb2xsZXIgaW1wbGVtZW50cyBuZy5JQ29udHJvbGxlciB7XG4gIHB1YmxpYyBzdGF0aWMgJGluamVjdDogc3RyaW5nW10gPSBbXG4gICAgJyRzdGF0ZScsXG4gICAgJ3BhcmFtZXRyb1NlcnZpY2UnXG4gIF1cbiAgcHJpdmF0ZSBfdGl0bGU6IHN0cmluZ1xuICBwcml2YXRlIF9wYXJhbWV0cm9zIDogQXJyYXk8SVBhcmFtZXRybz47XG4gIFxuICBwdWJsaWMgZ2V0IHBhcmFtZXRyb3MoKSA6IEFycmF5PElQYXJhbWV0cm8+IHtcbiAgICByZXR1cm4gdGhpcy5fcGFyYW1ldHJvcztcbiAgfVxuICBwdWJsaWMgc2V0IHBhcmFtZXRyb3ModiA6IEFycmF5PElQYXJhbWV0cm8+KSB7XG4gICAgdGhpcy5fcGFyYW1ldHJvcyA9IHY7XG4gIH1cbiAgcHVibGljIGdldCB0aXRsZSgpOiBzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLl90aXRsZVxuICB9XG4gIHB1YmxpYyBzZXQgdGl0bGUodjogc3RyaW5nKSB7XG4gICAgdGhpcy5fdGl0bGUgPSB2XG4gIH1cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSAkc3RhdGU6IElTdGF0ZVNlcnZpY2UgJiB7IGN1cnJlbnQ6IHsgZGF0YTogRGF0YSB9IH0sXG4gICAgcHJpdmF0ZSBwYXJhbWV0cm9TZXJ2aWNlOiBQYXJhbWV0cm9TZXJ2aWNlXG4gICkge1xuICAgIHRoaXMudGl0bGUgPSAkc3RhdGUuY3VycmVudC5kYXRhLnRpdGxlXG4gICAgdGhpcy5wYXJhbWV0cm9TZXJ2aWNlLmxpc3QoKS50aGVuKCByZXNwb25zZSA9PiB0aGlzLnBhcmFtZXRyb3MgPSByZXNwb25zZS5kYXRhIClcbiAgfVxufVxuIiwiaW1wb3J0IHsgQXJxdWl2b0NvbnRyb2xsZXIgfSBmcm9tICcuL0FycXVpdm9Db250cm9sbGVyJ1xuaW1wb3J0IHsgY29uZmlnLCBTdGF0ZSB9IGZyb20gJy4vLi4vc3lvbmV0L2NvcmUnXG5pbXBvcnQgeyBJVXJsUm91dGVyUHJvdmlkZXIsIElTdGF0ZVByb3ZpZGVyIH0gZnJvbSAnYW5ndWxhci11aS1yb3V0ZXInXG5pbXBvcnQgeyBUcmFkdWNhb0ZhY3RvcnkgfSBmcm9tICcuLi90cmFuc2xhdGUvVHJhZHVjYW9GYWN0b3J5J1xuXG5leHBvcnQgY2xhc3MgVmlld0luamVjdCB7XG4gIHB1YmxpYyBzdGF0aWMgcmVnaXN0ZXIoYXBwOiBuZy5JTW9kdWxlKTogbmcuSU1vZHVsZSB7XG4gICAgcmV0dXJuIGFwcFxuICAgICAgLmNvbnRyb2xsZXIoJ0FycXVpdm9Db250cm9sbGVyJywgQXJxdWl2b0NvbnRyb2xsZXIpXG4gICAgICAuY29uZmlnKFsnJHVybFJvdXRlclByb3ZpZGVyJywgJyRzdGF0ZVByb3ZpZGVyJywgJyR0cmFuc2xhdGVQcm92aWRlcicsKFxuICAgICAgICAkdXJsUm91dGVyUHJvdmlkZXI6IElVcmxSb3V0ZXJQcm92aWRlciAsIFxuICAgICAgICAkc3RhdGVQcm92aWRlcjogSVN0YXRlUHJvdmlkZXIsXG4gICAgICAgICR0cmFuc2xhdGVQcm92aWRlcjogYW5ndWxhci50cmFuc2xhdGUuSVRyYW5zbGF0ZVByb3ZpZGVyICkgPT4ge1xuICAgICAgICAkdHJhbnNsYXRlUHJvdmlkZXIudXNlU2FuaXRpemVWYWx1ZVN0cmF0ZWd5KCdlc2NhcGUnKVxuICAgICAgICAkdHJhbnNsYXRlUHJvdmlkZXIucHJlZmVycmVkTGFuZ3VhZ2UoJ1BULUJSJylcbiAgICAgICAgJHRyYW5zbGF0ZVByb3ZpZGVyLnVzZUxvYWRlcignY3VzdG9tTG9hZGVyJylcbiAgICAgICAgY29uZmlnLmZvckVhY2goKHZhbHVlOiBTdGF0ZSwga2V5OiBzdHJpbmcpID0+IHtcbiAgICAgICAgICAkc3RhdGVQcm92aWRlci5zdGF0ZShrZXksIHZhbHVlKVxuICAgICAgICB9KVxuICAgICAgICAkdXJsUm91dGVyUHJvdmlkZXIub3RoZXJ3aXNlKCAnL2hpc3Rvcmljby9jbGllbnRlJyApO1xuICAgICAgfV0pXG4gICAgICAuZmFjdG9yeSgnY3VzdG9tTG9hZGVyJywgKCRxOiBhbmd1bGFyLklRU2VydmljZSwgJGh0dHA6IGFuZ3VsYXIuSUh0dHBTZXJ2aWNlKSA9PiB7XG4gICAgICAgIHJldHVybiAob3B0aW9uczogYW55KSA9PiB7XG4gICAgICAgICAgY29uc3QgdXJsOiBzdHJpbmcgPSAnL2FwaS9wYXJhbWV0cm8vJ1xuICAgICAgICAgIGNvbnN0IHByb21pc2UgPSAkaHR0cC5nZXQodXJsICsgJ0lESU9NQV9TSVNURU1BJylcbiAgICAgICAgICBsZXQgZGZkOiBhbmd1bGFyLklEZWZlcnJlZDxhbnk+ID0gJHEuZGVmZXIoKVxuICAgICAgICAgIHByb21pc2UudGhlbigocmVzcG9uc2U6IGFueSkgPT4ge1xuICAgICAgICAgICAgZGZkLnJlc29sdmUobmV3IFRyYWR1Y2FvRmFjdG9yeSgpLmNyZWF0ZShyZXNwb25zZS5kYXRhLnZhbG9yKSlcbiAgICAgICAgICB9KVxuICAgICAgICAgIHJldHVybiBkZmQucHJvbWlzZVxuICAgICAgICB9XG4gICAgICB9KVxuICB9XG59XG4iXX0=
